#ifndef BATTERY_H
#define BATTERY_H


class Battery
{
  public:
    Battery(void);

    void update(void);

    float get_voltage(void);
    float get_charge(void);

  private:
    float voltage;
    float charge_percent;
};

#endif // BATTERY_H
