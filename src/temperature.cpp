#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "definitions.h"
#include "temperature.h"

#define TEMP_SENSOR_PIN PA8

OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensor(&oneWire);

Temperature::Temperature(void)
{

}

void Temperature::setup(void)
{
  sensor.begin();
  // TODO: set resolution from config
}

void Temperature::update(void)
{
  // TODO: this could use async mode and only update if previous call completed
  sensor.requestTemperatures();
  temperature = sensor.getTempCByIndex(0);
}

float Temperature::get_temperature(void)
{
  return temperature;
}
