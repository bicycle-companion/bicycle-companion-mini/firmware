#ifndef COMPONENTS_H
#define COMPONENTS_H

#include <Arduino.h>
#include "odometer.h"
#include "ui.h"
#include "battery.h"
#include "temperature.h"
#include "definitions.h"
#include "config.h"
#include <Button.h>
#include <STM32_Sleep.h>
#include <RTClock.h>

class Components
{
  public:
    Components(void) :
      buttons {
        Button(BUTTON_TL, 0, 0, 100),
        Button(BUTTON_TR, 0, 0, 100),
        Button(BUTTON_BL, 0, 0, 100),
        Button(BUTTON_BR, 0, 0, 100)
      },
      rtc(RTCSEL_LSE)
    {

    }

    Button buttons[(int)ButtonName::COUNT];
    Odometer odometer;
    UI ui;
    Battery battery;
    Temperature temperature;
    RTClock rtc;
    Config config;
};

extern Components* g_components;

#endif // COMPONENTS_H
