#include <Arduino.h>
#include "battery.h"

#define PIN_BATTERY PA4
#define MULTIPLY_RATIO (2.0f)
#define MAX_VOLTAGE (4.2f)
#define MIN_VOLTAGE (3.4f)
#define LOW_PASS_FACTOR (0.001f)
#define MAX_ANALOG_READ (4095)

Battery::Battery(void)
{
  pinMode(PIN_BATTERY, INPUT_ANALOG);
  voltage = MAX_VOLTAGE;
  charge_percent = 100.0f;
}

void Battery::update(void)
{
  float v = (float)analogRead(PIN_BATTERY) / (float)MAX_ANALOG_READ * 3.33f * 2.0f;
  voltage = LOW_PASS_FACTOR * v + (1 - LOW_PASS_FACTOR) * voltage;
  charge_percent = (fmaxf(voltage - MIN_VOLTAGE, 0.0f) / (MAX_VOLTAGE - MIN_VOLTAGE)) * 100.0f;
}

float Battery::get_voltage(void)
{
  return voltage;
}

float Battery::get_charge(void)
{
  return charge_percent;
}
