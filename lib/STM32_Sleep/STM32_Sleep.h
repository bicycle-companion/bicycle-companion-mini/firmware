#ifndef __STM32_SLEEP_H__
#define __STM32_SLEEP_H__

class STM32_Sleep
{
  public:
    /* sleep modes, in order of increasing sleep level */
    enum Mode
    {
      SLEEP, STOP, STOP_FAST_WAKEUP, STANDBY
    };

    static void init(void);

    static void sleep(void);
    static void stop(void);
    static void standby(void);

    static void set_mode(Mode mode);

    static void reenumerate_usb(void);

  private:
    static void reset_clock(void);
};

#endif
